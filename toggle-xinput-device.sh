#!/bin/bash

# NAME: toggle-xinput-device
# DESC: Toggle a device using xinput 
# CALL: 
# DATE: Created Dec 24, 2016.
# NOTE: Replace device by the xinput device you want
# device : Name of the device (you can get it with $ xinput list)

device='name';

if [ $(xinput list-props "$device" | awk '/Device Enabled/{print $NF}') -eq 0 ]; then
  sleep 0.1 ; xinput set-prop "$device" 'Device Enabled' 1;
  DISPLAY=:0 notify-send --urgency=critical "$device enabled"
else
  sleep 0.1 ; xinput set-prop "$device" 'Device Enabled' 0;
  DISPLAY=:0 notify-send --urgency=critical  "$device disabled"
fi

exit 0

