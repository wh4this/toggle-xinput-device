# toggle-xinput-device
A simple bash script used to toggle a device, it sends a notification

## How to use
Change the value of the variable "device" by the name of the device (list all device : $ xinput list) 
Then you can bind a keyboard shortcut to the script
